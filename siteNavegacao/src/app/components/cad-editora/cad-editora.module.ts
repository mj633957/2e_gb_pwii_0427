import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadEditoraRoutingModule } from './cad-editora-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CadEditoraRoutingModule
  ]
})
export class CadEditoraModule { }
